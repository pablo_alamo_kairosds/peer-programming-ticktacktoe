const player1 = 'x';
const player2 = 'o';
let currentPlayer = player1;
const boardState = Array(9).fill(null);
const solutions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

const cells = document.querySelectorAll('.btn');

cells.forEach((button, index) => {
  button.addEventListener('click', (e) => clickHandler(e, index));
});

const toggerPlayer = () => {
  currentPlayer = currentPlayer === player1 ? player2 : player1;
};

function clickHandler(event, index) {
  if (event.target.textContent === '') {
    event.target.innerHTML = currentPlayer;
    boardState[index] = currentPlayer;
    if (checkWinner()) {
      console.log(`${currentPlayer} has won!`);
    }
    toggerPlayer();
  }
}

function checkWinner() {
  return solutions.some(solution => solution.every(cell => boardState[cell] === currentPlayer));
}